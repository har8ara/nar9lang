import UIKit

func translateFromNar9Lang(text: String) -> String {
    var newText = text
    newText = newText.replacingOccurrences(of: "->", with: "ջ")
    
    newText = newText.replacingOccurrences(of: "a", with: "ն")
    newText = newText.replacingOccurrences(of: "b", with: "կ")
    
    newText = newText.replacingOccurrences(of: "c", with: " ")//
    
    newText = newText.replacingOccurrences(of: "d", with: "ի")
    newText = newText.replacingOccurrences(of: "e", with: "օ")
    newText = newText.replacingOccurrences(of: "f", with: "ու")
    newText = newText.replacingOccurrences(of: "g", with: "վ")
    newText = newText.replacingOccurrences(of: "h", with: "բ")
    newText = newText.replacingOccurrences(of: "i", with: "հ")
    newText = newText.replacingOccurrences(of: "j", with: "լ")
    newText = newText.replacingOccurrences(of: "k", with: "տ")
    
    newText = newText.replacingOccurrences(of: "l", with: " ")

    newText = newText.replacingOccurrences(of: "m", with: "ր")
    newText = newText.replacingOccurrences(of: "n", with: "ա")
    newText = newText.replacingOccurrences(of: "o", with: "ե")
    newText = newText.replacingOccurrences(of: "p", with: "խ")
    newText = newText.replacingOccurrences(of: "q", with: "ծ")
    newText = newText.replacingOccurrences(of: "r", with: "ժ")
    newText = newText.replacingOccurrences(of: "s", with: "գ")

    newText = newText.replacingOccurrences(of: "t", with: "և")//?

    newText = newText.replacingOccurrences(of: "u", with: "յ")
    newText = newText.replacingOccurrences(of: "v", with: "փ")
    newText = newText.replacingOccurrences(of: "w", with: "ց")
    newText = newText.replacingOccurrences(of: "x", with: "դ")
    newText = newText.replacingOccurrences(of: "y", with: "ս")
    newText = newText.replacingOccurrences(of: "z", with: "ձ")

    newText = newText.replacingOccurrences(of: "զ", with: "զ")//
    
    newText = newText.replacingOccurrences(of: "E", with: "է")
    newText = newText.replacingOccurrences(of: "%", with: "ը")
    newText = newText.replacingOccurrences(of: "?", with: "թ")
    newText = newText.replacingOccurrences(of: "§", with: "ղ")
    newText = newText.replacingOccurrences(of: "ճ", with: "ճ")//
    newText = newText.replacingOccurrences(of: "=", with: "մ")
    newText = newText.replacingOccurrences(of: "✡︎", with: "շ")
    newText = newText.replacingOccurrences(of: "<", with: "ո")
    newText = newText.replacingOccurrences(of: "®", with: "չ")
    newText = newText.replacingOccurrences(of: ">", with: "պ")

    newText = newText.replacingOccurrences(of: "ռ", with: "ռ") //
    
    newText = newText.replacingOccurrences(of: "@", with: "ք")
    
    newText = newText.replacingOccurrences(of: "ֆ", with: "ֆ")//
    return newText
}

let message = "sdkoy omob gom->n>oy bnmxnwd @< emnsmd E->%, smgnq 19.04.06?: bnmxnwd f aoaw =d koynb ngojd n=mn>axgow wnabf?ufay nkn=aom<g @oզ >n✡︎k>naojf: paxmf= o= ?fuj kfm daz jdaoj @< >nin>na imo✡︎%(bn= imo✡︎knb%) Ey buna@f=: d= ->fx<, sdko= <m ✡︎nk fro§ oy, hnuw fզf= o= jdaoj >nin>na% Ex n=fm <sfx, Ex =oq ymkdx, >nuqnռ poj@dx f ®@an§ nmkn@dadx: ?fuj kfm jdao= @< ioanmnaa f @< hnkfk%: sdkoy, p<ykf=aomdw ✡︎nk rjnk o=, hnuw ?fuj kfm p<yknan= xn @oզ: =anwnq p<ykf=aoma Ej ?<§ kn= ✡︎f✡︎dd yfmh n=oanvmb®f=:"

print(translateFromNar9Lang(text: message))


func translateToNar9Lang(text: String) -> String {
    var newText = text
    newText = newText.replacingOccurrences(of: "ու", with: "f")
    newText = newText.replacingOccurrences(of: "ա", with: "n")
    newText = newText.replacingOccurrences(of: "բ", with: "h")
    newText = newText.replacingOccurrences(of: "գ", with: "s")
    newText = newText.replacingOccurrences(of: "դ", with: "x")
    newText = newText.replacingOccurrences(of: "ե", with: "o")
    newText = newText.replacingOccurrences(of: "զ", with: "զ")//
    newText = newText.replacingOccurrences(of: "է", with: "E")
    newText = newText.replacingOccurrences(of: "ը", with: "%")
    newText = newText.replacingOccurrences(of: "թ", with: "?")
    newText = newText.replacingOccurrences(of: "ժ", with: "r")
    newText = newText.replacingOccurrences(of: "ի", with: "d")
    newText = newText.replacingOccurrences(of: "լ", with: "j")
    newText = newText.replacingOccurrences(of: "խ", with: "p")
    newText = newText.replacingOccurrences(of: "ծ", with: "q")
    newText = newText.replacingOccurrences(of: "կ", with: "b")
    newText = newText.replacingOccurrences(of: "հ", with: "i")
    newText = newText.replacingOccurrences(of: "ձ", with: "z")
    newText = newText.replacingOccurrences(of: "ղ", with: "§")
    newText = newText.replacingOccurrences(of: "ճ", with: "ճ")//
    newText = newText.replacingOccurrences(of: "մ", with: "=")
    newText = newText.replacingOccurrences(of: "յ", with: "u")
    newText = newText.replacingOccurrences(of: "ն", with: "a")
    newText = newText.replacingOccurrences(of: "շ", with: "✡︎")
    newText = newText.replacingOccurrences(of: "ո", with: "<")
    newText = newText.replacingOccurrences(of: "չ", with: "®")
    newText = newText.replacingOccurrences(of: "պ", with: ">")
    newText = newText.replacingOccurrences(of: "ջ", with: "->")
    newText = newText.replacingOccurrences(of: "ռ", with: "ռ") //
    newText = newText.replacingOccurrences(of: "ս", with: "y")
    newText = newText.replacingOccurrences(of: "վ", with: "g")
    newText = newText.replacingOccurrences(of: "տ", with: "k")
    newText = newText.replacingOccurrences(of: "ր", with: "m")
    newText = newText.replacingOccurrences(of: "ց", with: "w")
    newText = newText.replacingOccurrences(of: "փ", with: "v")
    newText = newText.replacingOccurrences(of: "ք", with: "@")
    newText = newText.replacingOccurrences(of: "և", with: "f")
    newText = newText.replacingOccurrences(of: "օ", with: "e")
    newText = newText.replacingOccurrences(of: "ֆ", with: "ֆ")//
    return newText
}

//let message = ""
//let newText = translateToNar9Lang(text: message)
//print(newText)


